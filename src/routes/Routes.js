import React from 'react'
import {Route, Switch} from "react-router-dom";
import MainPage from '../views/MainPage';

export default function Routes() {
    return (
        <Switch>
            <Route path="/" component={MainPage} exact/>
            {/* <Route component={NotFound} /> */}
        </Switch>
    )
}
