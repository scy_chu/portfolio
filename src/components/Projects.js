import React from 'react';
import rps1 from '../style/images/rps1.png';
import eas1 from '../style/images/eas1.png';
import protra from '../style/images/protra.png';
import movapp from '../style/images/movapp.png';
import todo from '../style/images/todo.png';
export default function Projects() {

    return (
        <div className="projects">
            <div className="projects-container">
                <h1>Personal & Group Projects</h1>
                <div className="project-list">
                    <div className="project-portfolio">
                        <div className="glints">
                            {/* <h2>Glints x Binar Academy Project Portfolio</h2> */}
                            <div className="glints-projects">
                                <div className="project-container">
                                        <div className="picture">
                                            <img src= {protra} alt="Protra (Final Project)"/>
                                        </div>
                                        <div className="description">
                                            <h3>Final Project (PROTRA)</h3>
                                            <p>
                                            Protra is another popular project management app offering the feature of project time tracking.
                                            This app gathers information on your team’s work in the form of activity reports. 
                                            That’s a great way to analyze your team’s performance and work progress.
                                            Sign up now and start your journey with us.
                                            </p>
                                            <div className="buttons">
                                                <button><a href="https://gitlab.com/glints-academy-6/team-c/front-end" target="_blank" rel="noopener noreferrer">View Code</a></button>
                                                <button><a href="https://protra.herokuapp.com" target="_blank" rel="noopener noreferrer">View in Browser</a></button>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div className="glints-projects">
                                <div className="project-container">
                                    <div className="picture">
                                        <img src= {movapp} alt="Movie App (Mini project 2)"/>
                                    </div>
                                    <div className="description">
                                        <h3>Mini Project 2 (Movie App)</h3>
                                        <p>
                                        This movie app allows you to search newest and popular movies on the fly.
                                        Sign up now and join the vast community of movie admirers and professional movie reviewers.
                                        </p>
                                        <div className="buttons">
                                            <button><a href="https://gitlab.com/raihanrachman31/movieapp" target="_blank" rel="noopener noreferrer">View Code</a></button>
                                            <button><a href="https://movvix.netlify.app" target="_blank"rel="noopener noreferrer">View in Browser</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="glints-projects">
                            <div className="project-container">
                                    <div className="picture">
                                        <img src= {todo} alt="Todo App (Mini Project 1)"/>
                                    </div>
                                    <div className="description">
                                        <h3>Mini Project 1 (Todo App)</h3>
                                        <p>
                                        This todo app works exactly the same as other todo app.
                                        Create a new task to your to do list, and update them as you wish.
                                        Need to label which one is important? There is an important label for that.
                                        You can even filter your todo list by the importance and your completed one.
                                        Try it now :)
                                        </p>
                                        <div className="buttons">
                                            <button><a href="https://gitlab.com/scy_chu/todo-app" target="_blank" rel="noopener noreferrer">View Code</a></button>
                                            <button><a href="https://scychu-todoapp.netlify.app/" target="_blank" rel="noopener noreferrer">View in Browser</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="The Odin Project">
                            {/* <h2>The Odin Project Portfolio</h2> */}
                            <div className="odin-projects">
                                <div className="project-container">
                                        <div className="picture">
                                            <img src= {rps1} alt="Rock Paper Scissors"/>
                                        </div>
                                        <div className="description">
                                            <h3>Rock Paper Scissors</h3>
                                            <p>
                                            Rock paper scissors is a hand game usually played between two people. 
                                            In this case user will play against the computer.
                                            User will choose among three options which is rock, paper and scissors.
                                            The winner will get one point to their score.
                                            There will be 5 rounds and the winner will be announce at the last round.
                                            You can play again by clicking PLAY AGAIN.
                                            </p>
                                            <div className="buttons">
                                                <button><a href="https://github.com/scychu/RPS_Game" target="_blank" rel="noopener noreferrer">View Code</a></button>
                                                <button><a href="https://scychu.github.io/RPS_Game/" target="_blank" rel="noopener noreferrer">View in Browser</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div className="odin-projects">
                                <div className="project-container">
                                        <div className="picture">
                                            <img src= {eas1} alt="Etch a Sketch"/>
                                        </div>
                                        <div className="description">
                                            <h3>Etch a Sketch</h3>
                                            <p>
                                            Etch A Sketch is a mechanical drawing toy invented by André Cassagnes of France.
                                            Now brought to the world of technology and you can freely use it.
                                            10 x 10 square is the default grid template . You can easily resize it (4-100), clean the canvas or set the thickness.
                                            Enjoy this game and create a good drawing.
                                            </p>
                                            <div className="buttons">
                                                <button><a href="https://github.com/scychu/Etch-a-Sketch" target="_blank" rel="noopener noreferrer">View Code</a></button>
                                                <button><a href="https://scychu.github.io/Etch-a-Sketch/" target="_blank" rel="noopener noreferrer">View in Browser</a></button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
