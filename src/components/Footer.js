import React from 'react'
import mail from "../style/images/mail.svg";
import linkedin from "../style/images/linkedin.svg";
import github from "../style/images/github.svg";
import gitlab from "../style/images/gitlab.svg";
import '../style/MainPage.scss';
export default function Footer() {
    return (
        <div className="footer">
            <h2>Want to know me more? Let's connect or find me here!!</h2>
            <div className="connect">
                <div className="medsos">
                    <a href="https://www.linkedin.com/in/scychu/"><img src={linkedin} alt="Let's connect"/><h4>scychu</h4></a>
                </div>
                <div className="medsos">
                    <a href="mailto: scajaboleh@gmail.com"><img src={mail} alt="Contact me"/><h4>scajaboleh@gmail.com</h4></a>
                </div>
                <div className="medsos">
                    <a href="https://github.com/scychu"><img src={github} alt="Follow me"/><h4>scychu</h4></a>
                </div>
                <div className="medsos">
                    <a href="https://gitlab.com/scy_chu"><img src={gitlab} alt="Follow me"/><h4>scy_chu</h4></a>
                </div>
            </div>
        </div>
    )
}
