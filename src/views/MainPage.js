import React from 'react'
import Navbar from '../components/Navbar'
import Introduction from '../components/Introduction'
import Projects from '../components/Projects'
import Footer from '../components/Footer'

export default function MainPage() {
    return (
        <div>
            <Navbar/>
            <Introduction/>
            <Projects/>
            <Footer/>
        </div>
    )
}
